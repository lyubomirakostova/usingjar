package com.company;

import com.company.annotations.FinishTests;
import com.company.annotations.MyTest;
import com.company.annotations.StartTests;

@StartTests(startMessage = "My tests :)")
@FinishTests
public class Test {

    @MyTest
    public static void test() {
        System.out.println("hey");
    }
}
